within greenhouse;

model Ventil_air_ext_air_int
  Ventilation ventilation annotation(
    Placement(transformation(origin = {-22, 14}, extent = {{-10, -10}, {10, 10}})));
  Air_ext air_ext annotation(
    Placement(transformation(origin = {28, 14}, extent = {{-10, -10}, {10, 10}})));
  Air_int air_int annotation(
    Placement(transformation(origin = {-76, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-80, 80}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(air_ext.T, ventilation.T) annotation(
    Line(points = {{18, 14}, {-10, 14}}, color = {0, 0, 127}));
  connect(air_int.port_a, ventilation.solid) annotation(
    Line(points = {{-68, 14}, {-32, 14}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Ventil_air_ext_air_int;
