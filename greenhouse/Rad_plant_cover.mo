within greenhouse;

model Rad_plant_cover
  Plant plant annotation(
    Placement(transformation(origin = {54, -16}, extent = {{-10, -10}, {10, 10}})));
  Cover cover annotation(
    Placement(transformation(origin = {-42, -14}, extent = {{-10, -10}, {10, 10}})));
  Radiation_plant_cover radiation_plant_cover annotation(
    Placement(transformation(origin = {10, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-72, 50}, extent = {{-10, -10}, {10, 10}})));
equation
 connect(cover.port_a, radiation_plant_cover.port_b) annotation(
    Line(points = {{-32, -16}, {0, -16}}, color = {191, 0, 0}));
 connect(plant.port_a, radiation_plant_cover.port_a) annotation(
    Line(points = {{46, -16}, {20, -16}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_plant_cover;
