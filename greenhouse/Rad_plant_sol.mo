within greenhouse;

model Rad_plant_sol
  Sol sol annotation(
    Placement(transformation(origin = {-8, -38}, extent = {{-10, -10}, {10, 10}})));
  Plant plant annotation(
    Placement(transformation(origin = {-8, 50}, extent = {{-10, -10}, {10, 10}})));
  Radiation_plant_sol radiation_plant_sol annotation(
    Placement(transformation(origin = {-8, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
equation
  connect(plant.port_a, radiation_plant_sol.port_a) annotation(
    Line(points = {{-8, 40}, {-8, 10}}, color = {191, 0, 0}));
  connect(radiation_plant_sol.port_b, sol.port_a) annotation(
    Line(points = {{-8, -10}, {-8, -34}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_plant_sol;
