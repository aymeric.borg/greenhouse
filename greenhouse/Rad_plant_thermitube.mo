within greenhouse;

model Rad_plant_thermitube
  Radiation_plant_thermitube radiation_plant_thermitube annotation(
    Placement(transformation(origin = {-2, 2}, extent = {{-10, -10}, {10, 10}})));
  Plant plant annotation(
    Placement(transformation(origin = {-52, 12}, extent = {{-10, -10}, {10, 10}})));
  Thermitube thermitube annotation(
    Placement(transformation(origin = {46, 2}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(thermitube.port_a, radiation_plant_thermitube.port_b) annotation(
    Line(points = {{38, 2}, {8, 2}}, color = {191, 0, 0}));
  connect(plant.port_a, radiation_plant_thermitube.port_a) annotation(
    Line(points = {{-52, 2}, {-12, 2}}, color = {191, 0, 0}));
annotation(
    Icon(graphics = {Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_plant_thermitube;
