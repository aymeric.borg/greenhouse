within greenhouse;

model Cover
  Modelica.Units.SI.Temperature T(start=288.15);
  
  parameter Modelica.Units.SI.Length h_C = 0.004 "épaisseur de la paroi";
  Real C = cst.rho_cover * cst.Cp_cover * h_C / Modelica.Math.cos(cst.phi);
  outer Constantes cst annotation(
    Placement(transformation(origin = {-74, 74}, extent = {{-10, -10}, {10, 10}})));
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-88, 4}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {90, -30}, extent = {{-10, -10}, {10, 10}})));
equation

  T = port_a.T;
  C * der(T) = port_a.Qs_flow;
annotation(
    Icon(graphics = {Polygon(origin = {0, -40}, points = {{-80, -60}, {80, -60}, {80, 60}, {0, 100}, {-80, 60}})}));
end Cover;
