within greenhouse;

model Radiation_soleil_plant
  Modelica.Units.SI.HeatFlux Qs_flow;
  Interface.SurfHeatPort_b port_b annotation(
    Placement(transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-96, 2}, extent = {{-10, -10}, {10, 10}})));
  Modelica.Blocks.Interfaces.RealInput NIR annotation(
    Placement(transformation(origin = {40, 110}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {-40, 100}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealInput PAR annotation(
    Placement(transformation(origin = {-40, 110}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {40, 100}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput Rad_tot annotation(
    Placement(transformation(origin = {4, -96}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {0, -100}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  outer Constantes cst annotation(
    Placement(transformation(origin = {-90, 90}, extent = {{-10, -10}, {10, 10}})));
  
equation
  port_b.Qs_flow = Qs_flow;
  Qs_flow = - (PAR*(1-cst.alpha_air_PAR)*cst.tau_cover_PAR*cst.alpha_plant_PAR+NIR*(1-cst.alpha_air_NIR)*cst.tau_cover_NIR*cst.alpha_plant_NIR);
  Rad_tot=Qs_flow;
  
annotation(
    Icon(graphics = {Line(origin = {-0.45, 59.55}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {-0.8, -19.9}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {1.65, -60.15}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {1.65, 19.65}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}})}));
end Radiation_soleil_plant;
