within greenhouse;

model Rad_cover_thermitube
  Radiation_cover_thermitube radiation_cover_thermitube annotation(
    Placement(transformation(origin = {4, 2}, extent = {{-10, -10}, {10, 10}})));
  Cover cover annotation(
    Placement(transformation(origin = {-54, 4}, extent = {{-10, -10}, {10, 10}})));
  Thermitube thermitube annotation(
    Placement(transformation(origin = {70, 2}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(thermitube.port_a, radiation_cover_thermitube.port_b) annotation(
    Line(points = {{62, 2}, {14, 2}}, color = {191, 0, 0}));
  connect(cover.port_a, radiation_cover_thermitube.port_a) annotation(
    Line(points = {{-45, 1}, {-6, 1}, {-6, 2}}, color = {191, 0, 0}));
annotation(
    Icon(graphics = {Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_cover_thermitube;
