within greenhouse;

model Assemblage
  extends Modelica.Icons.Example;
  Plant plant(T(start = 288.15))  annotation(
    Placement(transformation(origin = {-21, -17}, extent = {{-7, -7}, {7, 7}})));
  Air_int air_int annotation(
    Placement(transformation(origin = {88, 26}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_int_cover convection_air_int_cover annotation(
    Placement(transformation(origin = {50, 26}, extent = {{-10, -10}, {10, 10}})));
  Cover cover(T(start = 278.15))  annotation(
    Placement(transformation(origin = {2, 30}, extent = {{-10, -10}, {10, 10}})));
  Radiation_plant_cover radiation_plant_cover annotation(
    Placement(transformation(origin = {12, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Radiation_atmo_cover radiation_atmo_cover annotation(
    Placement(transformation(origin = {-60, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Blocks.Sources.Constant T_sky(k = 5) annotation(
    Placement(transformation(origin = {-88, 60}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_ext_cover convection_air_ext_cover annotation(
    Placement(transformation(origin = {42, 76}, extent = {{-10, -10}, {10, 10}})));
  Sol sol annotation(
    Placement(transformation(origin = {-46, -92}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_int_sol convection_air_int_sol annotation(
    Placement(transformation(origin = {50, -86}, extent = {{-10, -10}, {10, 10}})));
  Soleil soleil annotation(
    Placement(transformation(origin = {-92, -20}, extent = {{-10, -10}, {10, 10}})));
  Radiation_soleil_plant radiation_soleil_plant annotation(
    Placement(transformation(origin = {-44, -18}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Radiation_soleil_cover radiation_soleil_cover annotation(
    Placement(transformation(origin = {-58, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Air_ext air_ext annotation(
    Placement(transformation(origin = {78, 76}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_int_plant convection_air_int_plant annotation(
    Placement(transformation(origin = {45, -25}, extent = {{-9, -9}, {9, 9}})));
  Ventilation ventilation annotation(
    Placement(transformation(origin = {64, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-84, -76}, extent = {{-10, -10}, {10, 10}})));
  Radiation_plant_sol radiation_plant_sol annotation(
    Placement(transformation(origin = {-46, -60}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Radiation_cover_sol radiation_cover_sol annotation(
    Placement(transformation(origin = {28, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Radiation_soleil_sol radiation_soleil_sol annotation(
    Placement(transformation(origin = {-64, -48}, extent = {{-10, -10}, {10, 10}})));
  Modelica.Blocks.Sources.Constant T_air_sol(k = 13.5) annotation(
    Placement(transformation(origin = {90, -90}, extent = {{-6, -6}, {6, 6}}, rotation = 90)));
  Convection_air_int_thermitube convection_air_int_thermitube annotation(
    Placement(transformation(origin = {65, -63}, extent = {{-7, -7}, {7, 7}})));
  Radiation_plant_thermitube radiation_plant_thermitube annotation(
    Placement(transformation(origin = {9, -41}, extent = {{-7, -7}, {7, 7}}, rotation = -90)));
  Radiation_soleil_thermitube radiation_soleil_thermitube annotation(
    Placement(transformation(origin = {-3, -43}, extent = {{-7, -7}, {7, 7}}, rotation = 90)));
  Radiation_sol_thermitube radiation_sol_thermitube annotation(
    Placement(transformation(origin = {-13, -75}, extent = {{-9, -9}, {9, 9}})));
  Radiation_cover_thermitube radiation_cover_thermitube annotation(
    Placement(transformation(origin = {23, -55}, extent = {{-7, -7}, {7, 7}})));
  Evapotranspiration evapotranspiration annotation(
    Placement(transformation(origin = {-22, 6}, extent = {{-10, -10}, {10, 10}})));
  Radiation_soleil_air radiation_soleil_air annotation(
    Placement(transformation(origin = {-14, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  C_puits_canadien c_puits_canadien annotation(
    Placement(transformation(origin = {90, -56}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  T t annotation(
    Placement(transformation(origin = {7, -59}, extent = {{-7, -7}, {7, 7}})));
  Switch switch annotation(
    Placement(transformation(origin = {-14, 68}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(T_sky.y, radiation_atmo_cover.T) annotation(
    Line(points = {{-77, 60}, {-70, 60}}, color = {0, 0, 127}));
  connect(radiation_soleil_plant.PAR, soleil.PAR) annotation(
    Line(points = {{-48, -28}, {-68, -28}, {-68, -14}, {-80, -14}}, color = {0, 0, 127}));
  connect(radiation_soleil_plant.NIR, soleil.NIR) annotation(
    Line(points = {{-40, -28}, {-76, -28}, {-76, -24}, {-80, -24}}, color = {0, 0, 127}));
  connect(radiation_soleil_cover.PAR, soleil.PAR) annotation(
    Line(points = {{-62, 8}, {-62, -14}, {-80, -14}}, color = {0, 0, 127}));
  connect(radiation_soleil_cover.NIR, soleil.NIR) annotation(
    Line(points = {{-54, 8}, {-54, -18}, {-76, -18}, {-76, -24}, {-80, -24}}, color = {0, 0, 127}));
  connect(convection_air_ext_cover.T, air_ext.T) annotation(
    Line(points = {{54, 76}, {67, 76}}, color = {0, 0, 127}));
  connect(air_int.port_a, convection_air_int_cover.fluid) annotation(
    Line(points = {{80, 26}, {80, 25}, {58, 25}, {58, 24}}, color = {191, 0, 0}));
  connect(cover.port_a, convection_air_int_cover.solid) annotation(
    Line(points = {{11, 27}, {40, 27}, {40, 26}}, color = {191, 0, 0}));
  connect(radiation_plant_cover.port_b, cover.port_a) annotation(
    Line(points = {{12, -2}, {12, 12}, {11, 12}, {11, 27}}, color = {191, 0, 0}));
  connect(convection_air_int_sol.fluid, air_int.port_a) annotation(
    Line(points = {{60, -86}, {80, -86}, {80, 26}}, color = {191, 0, 0}));
  connect(convection_air_int_sol.solid, sol.port_a) annotation(
    Line(points = {{40, -86}, {8, -86}, {8, -86.5}, {-46, -86.5}, {-46, -89}}, color = {191, 0, 0}));
  connect(plant.port_a, radiation_plant_cover.port_a) annotation(
    Line(points = {{-21, -24}, {10, -24}, {10, -22}, {12, -22}}, color = {191, 0, 0}));
  connect(radiation_soleil_plant.port_b, plant.port_a) annotation(
    Line(points = {{-34, -18}, {-28.5, -18}, {-28.5, -24}, {-21, -24}}, color = {191, 0, 0}));
  connect(radiation_soleil_cover.port_b, cover.port_a) annotation(
    Line(points = {{-48, 18}, {11, 18}, {11, 27}}, color = {191, 0, 0}));
  connect(convection_air_ext_cover.solid, cover.port_a) annotation(
    Line(points = {{32, 76}, {11, 76}, {11, 27}}, color = {191, 0, 0}));
  connect(convection_air_int_plant.fluid, air_int.port_a) annotation(
    Line(points = {{53, -27}, {80, -27}, {80, 26}}, color = {191, 0, 0}));
  connect(plant.port_a, convection_air_int_plant.solid) annotation(
    Line(points = {{-21, -24}, {7.5, -24}, {7.5, -25}, {36, -25}}, color = {191, 0, 0}));
  connect(air_ext.T, ventilation.T) annotation(
    Line(points = {{68, 76}, {68, 74}, {64, 74}, {64, 66}}, color = {0, 0, 127}));
  connect(ventilation.solid, air_int.port_a) annotation(
    Line(points = {{64, 44}, {64, 26}, {80, 26}}, color = {191, 0, 0}));
  connect(plant.port_a, radiation_plant_sol.port_a) annotation(
    Line(points = {{-21, -24}, {-14, -24}, {-14, -34}, {-46, -34}, {-46, -50}}, color = {191, 0, 0}));
  connect(radiation_plant_sol.port_b, sol.port_a) annotation(
    Line(points = {{-46, -70}, {-46, -89}}, color = {191, 0, 0}));
  connect(radiation_cover_sol.port_b, cover.port_a) annotation(
    Line(points = {{28, 16}, {28, 28}, {12, 28}}, color = {191, 0, 0}));
  connect(radiation_soleil_sol.NIR, soleil.NIR) annotation(
    Line(points = {{-68, -38}, {-68, -30}, {-76, -30}, {-76, -24}, {-80, -24}}, color = {0, 0, 127}));
  connect(radiation_soleil_sol.PAR, soleil.PAR) annotation(
    Line(points = {{-60, -38}, {-60, -24}, {-72, -24}, {-72, -14}, {-80, -14}}, color = {0, 0, 127}));
  connect(radiation_soleil_sol.port_b, sol.port_a) annotation(
    Line(points = {{-64, -58}, {-64, -89}, {-46, -89}}, color = {191, 0, 0}));
  connect(radiation_cover_sol.port_a, sol.port_a) annotation(
    Line(points = {{28, -4}, {-31, -4}, {-31, -48}, {-32, -48}, {-32, -74}, {-46, -74}, {-46, -89}}, color = {191, 0, 0}));
  connect(convection_air_int_thermitube.fluid, air_int.port_a) annotation(
    Line(points = {{71, -65}, {80, -65}, {80, 26}}, color = {191, 0, 0}));
  connect(radiation_plant_thermitube.port_a, plant.port_a) annotation(
    Line(points = {{9, -34}, {8, -34}, {8, -24}, {-20, -24}}, color = {191, 0, 0}));
  connect(radiation_soleil_thermitube.NIR, soleil.NIR) annotation(
    Line(points = {{-10, -46}, {-55, -46}, {-55, -24}, {-80, -24}}, color = {0, 0, 127}));
  connect(radiation_soleil_thermitube.PAR, soleil.PAR) annotation(
    Line(points = {{-10, -40}, {-64, -40}, {-64, -15}, {-80, -15}, {-80, -14}}, color = {0, 0, 127}));
  connect(radiation_sol_thermitube.port_a, sol.port_a) annotation(
    Line(points = {{-22, -74}, {-46, -74}, {-46, -88}}, color = {191, 0, 0}));
  connect(cover.port_a, radiation_cover_thermitube.port_b) annotation(
    Line(points = {{12, 28}, {20, 28}, {20, -13}, {30, -13}, {30, -54}}, color = {191, 0, 0}));
  connect(radiation_soleil_plant.Rad_tot, evapotranspiration.Rad_tot) annotation(
    Line(points = {{-44, -8}, {-44, 6}, {-30, 6}}, color = {0, 0, 127}));
  connect(evapotranspiration.port_b, plant.port_a) annotation(
    Line(points = {{-12, 6}, {-6, 6}, {-6, -24}, {-20, -24}}, color = {191, 0, 0}));
  connect(soleil.PAR, radiation_soleil_air.PAR) annotation(
    Line(points = {{-80, -14}, {-70, -14}, {-70, 52}, {-24, 52}}, color = {0, 0, 127}));
  connect(soleil.NIR, radiation_soleil_air.NIR) annotation(
    Line(points = {{-80, -24}, {-68, -24}, {-68, 44}, {-24, 44}}, color = {0, 0, 127}));
  connect(radiation_soleil_air.port_b, air_int.port_a) annotation(
    Line(points = {{-14, 38}, {64, 38}, {64, 26}, {80, 26}}, color = {191, 0, 0}));
  connect(T_air_sol.y, c_puits_canadien.E) annotation(
    Line(points = {{90, -84}, {90, -66}}, color = {0, 0, 127}));
  connect(c_puits_canadien.port_a, air_int.port_a) annotation(
    Line(points = {{90, -46}, {90, -26}, {80, -26}, {80, 26}}, color = {191, 0, 0}));
  connect(t.port_a, radiation_sol_thermitube.port_b) annotation(
    Line(points = {{0, -58}, {-4, -58}, {-4, -74}}, color = {191, 0, 0}));
  connect(t.port_a, radiation_soleil_thermitube.port_b) annotation(
    Line(points = {{0, -58}, {-4, -58}, {-4, -50}}, color = {191, 0, 0}));
  connect(t.port_a, radiation_plant_thermitube.port_b) annotation(
    Line(points = {{0, -58}, {0, -48}, {10, -48}}, color = {191, 0, 0}));
  connect(t.port_a, radiation_cover_thermitube.port_a) annotation(
    Line(points = {{0, -58}, {16, -58}, {16, -54}}, color = {191, 0, 0}));
  connect(convection_air_int_thermitube.solid, t.port_a) annotation(
    Line(points = {{58, -62}, {0, -62}, {0, -58}}, color = {191, 0, 0}));
  connect(radiation_atmo_cover.port_a, switch.port_a) annotation(
    Line(points = {{-50, 60}, {-24, 60}, {-24, 68}}, color = {191, 0, 0}));
  connect(switch.port_b, cover.port_a) annotation(
    Line(points = {{-4, 68}, {12, 68}, {12, 28}}, color = {191, 0, 0}));
  annotation(
    Diagram);end Assemblage;
