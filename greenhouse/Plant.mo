within greenhouse;

model Plant

  Modelica.Units.SI.Temperature T(start=293.15) "Température de la plante (en Kelvin)";
  parameter Unit.HeatCapacitySurf c=1200 "capacité thermique surfacique de la plante";
  parameter Unit.HeatCapacitySurf C=c*cst.LAI "capacité thermique surfacique de l'ensemble des plantes de la serre";
  outer Constantes cst annotation(
    Placement(transformation(origin = {-68, 58}, extent = {{-10, -10}, {10, 10}})));
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-80, -2}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {0, -96}, extent = {{-10, -10}, {10, 10}})));
equation
   T = port_a.T;
   C*der(T)=port_a.Qs_flow;

annotation(
    Diagram,
Icon(graphics = {Line(origin = {0.15, -33.25}, points = {{0, 51}, {0, -51}, {0, -51}, {0, -51}}, color = {0, 170, 0}, thickness = 2), Ellipse(origin = {-32, 19}, lineColor = {0, 170, 0}, fillColor = {0, 170, 0}, fillPattern = FillPattern.Solid, extent = {{32, -13}, {-32, 13}}), Ellipse(origin = {32, 19}, lineColor = {0, 170, 0}, fillColor = {0, 170, 0}, fillPattern = FillPattern.Solid, extent = {{32, -13}, {-32, 13}})}));
end Plant;
