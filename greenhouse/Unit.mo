within greenhouse;

package Unit
    type HeatCapacitySurf = Real(unit = "J/(K.m2)");

  type HeatCapacityVol = Real(unit = "J/(K.L)");
end Unit;
