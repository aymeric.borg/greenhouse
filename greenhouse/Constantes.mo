within greenhouse;

model Constantes
  parameter Modelica.Units.SI.Area S_sol(fixed=true) = 1000; 
  parameter Modelica.Units.SI.Area S_thermitube(fixed=true) = 256 "surface totale des thermitubes (pour 19 rangées de 50m pour des thermitubes de 27cm de diamètre)";
  parameter Modelica.Units.SI.Angle phi = 15*3.14/180 "angle d'incidence des rayons sur la paroi";
  
  parameter Modelica.Units.SI.VolumeFlowRate Dv = 0.1852 "Débit volumique de l'air échangé par la ventilation";
  parameter Modelica.Units.SI.SpecificHeatCapacity C_air = 1005 "capacité thermique massique de l'air intérieur";
  parameter Modelica.Units.SI.Density rho_air = 1.2 "masse volumique de l'air intérieur";
  Real U_ventilation = Dv*C_air*rho_air/S_sol;
  parameter Modelica.Units.SI.Density rho_cover = 2600 "masse volumique de la paroi (verre = 2600, PET = 900)";
  parameter Modelica.Units.SI.SpecificHeatCapacity Cp_cover = 840 "capacité thermique massique de la paroi (verre = 840, PET = 1700)";

  parameter Real LAI = 2.7 "Leaf Area Index";
  parameter Real k1 = 0.94;
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer alpha_Leaf_Air = 5 "Coefficient de transfert thermique en W/(K.m²)";
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer U_plant_air = 2*alpha_Leaf_Air*LAI "Coefficient de transfert de chaleur entre la plante et l'air intérieur";
  
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer U_air_int_cover = 1.7*Modelica.Math.cos(phi)^(-0.66) "Coefficient de transfert de chaleur entre la paroi et l'air intérieur";

  Real F_cover_plant "Coefficient de vision serre-plantes";
  Real F_cover_sol "Coefficient de vision serre-sol";
  Real F_plant_sol "Coefficient de vision plantes-sol";
  Real F_plant_thermitube "coefficient de vision plantes-thermitubes";
  Real F_sol_thermitube "coefficient de vision sol-thermitubes";
  Real F_cover_thermitube "coefficient de vision serre-thermitubes";
  parameter Real F_cover_sky = 1 "coefficient de vision";
  parameter Real k_NIR = 0.27;
  parameter Real eps_cover = 0.85 "coefficient d'émissivité de la cover: verre = 0.85 ; Polyéthylène = 0.92";
  parameter Real eps_plant = 1 "coefficient d'émissivité de la plante";
  parameter Real eps_sol = 0.90 "coefficient d'émissivité du sol";
  parameter Real eps_atmo = 0.95 "coefficient approché de l'émissivité de l'atmosphère";
  parameter Real eps_thermitube = 1 "coefficient d'émissivité des thermotubes (à rectifier)";
 
  parameter Real tau_cover_PAR=0.79 "la proportion de PAR transmis à travers la surface";
  parameter Real alpha_cover_PAR=0.04 " la proportion de PAR absorbée par la surface";
  parameter Real tau_cover_NIR=0.75 "la proportion de NIR transmis à travers la surface";
  parameter Real alpha_cover_NIR=0.04 " la proportion de NIR absorbée par la surface";

  parameter Real alpha_plant_PAR=0.95-0.9*Modelica.Math.exp(-0.85*LAI) "la proportion de PAR absorbé par les plantes";    
  parameter Real alpha_plant_NIR=0.65-0.65*Modelica.Math.exp(-0.27*LAI) "la proportion de NIR absorbé par les plantes";
    
  parameter Real alpha_sol_PAR=0.05+0.91*Modelica.Math.exp(-0.5*LAI) "la proportion de PAR absorbé par le sol";
  parameter Real alpha_sol_NIR=Modelica.Math.exp(-0.92*LAI) "la proportion de NIR absorbé par le sol";
  
  parameter Real alpha_air_PAR=0.06;
  parameter Real alpha_air_NIR=0.06;
  
  parameter Modelica.Units.SI.Diameter D_thermitube = 0.25 "diamètre des thermitubes en m";
  
equation
  F_cover_plant = 1 - Modelica.Math.exp(-k1*LAI);
  F_cover_sol = Modelica.Math.exp(-k1*LAI)*(S_sol-S_thermitube)/S_sol;
  F_cover_thermitube = Modelica.Math.exp(-k1*LAI)*S_thermitube/S_sol;
  
  F_plant_sol = (1 - Modelica.Math.exp(-k1*LAI))*(S_sol-S_thermitube)/S_sol;
  F_plant_thermitube = 1 - Modelica.Math.exp(-k1*LAI)*S_thermitube/S_sol;
  F_sol_thermitube = S_thermitube/S_sol;
  
annotation(
    Icon(graphics = {Text(extent = {{-100, 100}, {100, -100}}, textString = "C")}));
end Constantes;
