within greenhouse;

model Radiation_cover_thermitube
Modelica.Units.SI.HeatFlux Qs_flow;
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-94, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-102, 0}, extent = {{-10, -10}, {10, 10}})));
  Interface.SurfHeatPort_b port_b annotation(
    Placement(transformation(origin = {92, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}})));
  Real Gc = cst.eps_cover*cst.eps_thermitube*cst.F_cover_thermitube*Modelica.Constants.sigma ;
  outer Constantes cst annotation(
    Placement(transformation(origin = {-54, 64}, extent = {{-10, -10}, {10, 10}})));
equation
  port_a.Qs_flow=Qs_flow;
  port_b.Qs_flow=-Qs_flow;
  Qs_flow=Gc*(port_a.T^4-port_b.T^4);
annotation(
    Icon(graphics = {Line(origin = {-0.45, 59.55}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {-0.8, -19.9}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {1.65, -60.15}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {1.65, 19.65}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}})}));


end Radiation_cover_thermitube;
