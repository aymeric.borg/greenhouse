within greenhouse;

model Conv_air_int_plant
  Air_int air_int annotation(
    Placement(transformation(origin = {72, 18}, extent = {{-10, -10}, {10, 10}})));
  Plant plant annotation(
    Placement(transformation(origin = {-52, 30}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_int_plant convection_air_int_plant annotation(
    Placement(transformation(origin = {8, 20}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(plant.port_a, convection_air_int_plant.solid) annotation(
    Line(points = {{-52, 20}, {-2, 20}}, color = {191, 0, 0}));
  connect(convection_air_int_plant.fluid, air_int.port_a) annotation(
    Line(points = {{16, 18}, {64, 18}}, color = {191, 0, 0}));

annotation(
    Diagram,
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Conv_air_int_plant;
