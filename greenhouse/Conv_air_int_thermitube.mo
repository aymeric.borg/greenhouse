within greenhouse;

model Conv_air_int_thermitube
  Convection_air_int_thermitube convection_air_int_thermitube annotation(
    Placement(transformation(origin = {2, -2}, extent = {{-10, -10}, {10, 10}})));
  thermitube thermitube1 annotation(
    Placement(transformation(origin = {-54, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Air_int air_int annotation(
    Placement(transformation(origin = {58, -4}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(thermitube1.port_a, convection_air_int_thermitube.solid) annotation(
    Line(points = {{-46, -2}, {-8, -2}}, color = {191, 0, 0}));
  connect(convection_air_int_thermitube.fluid, air_int.port_a) annotation(
    Line(points = {{10, -4}, {50, -4}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Conv_air_int_thermitube;
