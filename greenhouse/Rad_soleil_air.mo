within greenhouse;

model Rad_soleil_air
  Soleil soleil annotation(
    Placement(transformation(origin = {-8, 56}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Radiation_soleil_cover radiation_soleil_cover annotation(
    Placement(transformation(origin = {-6, 2}, extent = {{-10, -10}, {10, 10}})));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-24, -44}, extent = {{-10, -10}, {10, 10}})));
  Air_int air_int annotation(
    Placement(transformation(origin = {-46, -6}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(soleil.NIR, radiation_soleil_cover.NIR) annotation(
    Line(points = {{-12, 45}, {-12, 27.5}, {-10, 27.5}, {-10, 12}}, color = {0, 0, 127}));
  connect(soleil.PAR, radiation_soleil_cover.PAR) annotation(
    Line(points = {{-2, 45}, {-2, 12}}, color = {0, 0, 127}));
  connect(radiation_soleil_cover.port_b, air_int.port_a) annotation(
    Line(points = {{-16, 2}, {-54, 2}, {-54, -6}}, color = {191, 0, 0}));
  annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));

end Rad_soleil_air;
