import pandas as pd

# Replace 'path/to/your/input/file.xlsx' with the path to your Excel file
excel_file_path = "I_GLOB_MODELICA.xlsx"

# Spécifier la ligne à partir de laquelle vous souhaitez récupérer les données
start_row = 10

# Read the Excel file into a DataFrame
df = pd.read_excel(excel_file_path, skiprows=range(1, start_row))


df.columns.values[0] = "double"
df.columns.values[1] = f"table({len(df)},2)"
df["double"] = range(0, len(df))

df = df[["double", f"table({len(df)},2)"]]

# Replace 'path/to/your/output/file.txt' with the desired path for the text file
text_file_path = "I_GLOB_MODELICA.txt"

# Save the DataFrame as a text file
df.to_csv(text_file_path, sep="\t", index=False)

# Lire toutes les lignes actuelles du fichier texte
with open(text_file_path, "r+") as f:
    lignes = f.readlines()

    # Insérer la nouvelle ligne au début du fichier
    nouvelle_ligne = "#1"
    lignes.insert(0, nouvelle_ligne + "\n")

    # Retour au début du fichier pour écrire les modifications
    f.seek(0)

    # Écrire toutes les lignes mises à jour dans le fichier
    f.writelines(lignes)
