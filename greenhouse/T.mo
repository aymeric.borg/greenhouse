within greenhouse;

model T
  Thermitube thermitube annotation(
    Placement(transformation(origin = {44, -6}, extent = {{-10, -10}, {10, 10}})));
  Switch switch_thermitube annotation(
    Placement(transformation(origin = {-18, -6}, extent = {{-10, -10}, {10, 10}})));
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-104, -6}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(thermitube.port_a, switch_thermitube.port_b) annotation(
    Line(points = {{36, -6}, {-8, -6}}, color = {191, 0, 0}));
  connect(switch_thermitube.port_a, port_a) annotation(
    Line(points = {{-28, -6}, {-104, -6}}, color = {191, 0, 0}));
annotation(
    Icon(graphics = {Rectangle(origin = {-1, 13}, fillPattern = FillPattern.Solid, extent = {{-75, 7}, {75, -7}}), Rectangle(origin = {-1, -13}, fillPattern = FillPattern.Solid, extent = {{-75, 7}, {75, -7}})}));
end T;
