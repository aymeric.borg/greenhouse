within greenhouse;

model Air_int
  Modelica.Units.SI.Temperature T(start=283.15) "température à l'intérieur de la serre"; 
  parameter Modelica.Units.SI.Density rho = 1.2 "densité de l'air";
  parameter Modelica.Units.SI.SpecificHeatCapacity c = 1000 "capacité thermique massique de l'air";
  parameter Modelica.Units.SI.Length h=4 "hauteur de la serre au point bas";
  Real C = rho*c*h;
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-18, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-84, 2}, extent = {{-10, -10}, {10, 10}})));
equation
  T = port_a.T;
  C*der(T)=port_a.Qs_flow;
  

annotation(
    Icon(graphics = {Line(origin = {-7.07, 27}, points = {{-64.9319, -19}, {49.0681, -19}, {65.0681, -5}, {51.0681, 19}, {29.0681, 19}, {29.0681, 19}, {29.0681, 19}}, thickness = 2.25), Line(origin = {-7.15, -22}, points = {{-64.8536, 16}, {47.1464, 16}, {65.1464, 2}, {47.1464, -16}, {31.1464, -16}, {33.1464, -16}}, thickness = 2)}));
end Air_int;
