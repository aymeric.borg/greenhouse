within greenhouse;

model Sol

  parameter Modelica.Units.SI.ThermalConductivity lambda = 0.85 "conductivité thermique du sol";
  
  parameter Modelica.Units.SI.Length e = 0.01 "epaisseur de la couche de sol 1";
  parameter Modelica.Units.SI.Length e2 = 0.04 "epaisseur de la couche de sol 2";
  parameter Modelica.Units.SI.Length e3 = 0.08 "epaisseur de la couche de sol 3";  
  parameter Modelica.Units.SI.Length e4 = 0.16 "epaisseur de la couche de sol 4";
  parameter Modelica.Units.SI.Length e5 = 0.32 "epaisseur de la couche de sol 5";
  parameter Modelica.Units.SI.Length e6 = 0.64 "epaisseur de la couche de sol 6";
  parameter Modelica.Units.SI.Length e7 = 1.28 "epaisseur de la couche de sol 7";

  parameter Real U_floor = lambda/e "conductance thermique surfacique de la couche de sol 1";
  parameter Real U2_floor = lambda/e2 "conductance thermique surfacique de la couche de sol 2";
  parameter Real U3_floor = lambda/e3 "conductance thermique surfacique de la couche de sol 3";
  parameter Real U4_floor = lambda/e4 "conductance thermique surfacique de la couche de sol 4";
  parameter Real U5_floor = lambda/e5 "conductance thermique surfacique de la couche de sol 5";
  parameter Real U6_floor = lambda/e6 "conductance thermique surfacique de la couche de sol 6";
  parameter Real U7_floor = lambda/e7 "conductance thermique surfacique de la couche de sol 7";

  parameter Modelica.Units.SI.HeatCapacity C = 17.3*10^6 "capacité thermique de la couche de sol 1";
  parameter Modelica.Units.SI.HeatCapacity C2 = 69.2*10^6 "capacité thermique de la couche de sol 2";
  parameter Modelica.Units.SI.HeatCapacity C3 = 138*10^6 "capacité thermique de la couche de sol 3";
  parameter Modelica.Units.SI.HeatCapacity C4 = 277*10^6 "capacité thermique de la couche de sol 4";
  parameter Modelica.Units.SI.HeatCapacity C5 = 554*10^6 "capacité thermique de la couche de sol 5";
  parameter Modelica.Units.SI.HeatCapacity C6 = 1110*10^6 "capacité thermique de la couche de sol 6";
  parameter Modelica.Units.SI.HeatCapacity C7 = 2210*10^6 "capacité thermique de la couche de sol 7";
  
  parameter Real Csurf = C/cst.S_sol "capacité thermique surfacique de la couche de sol 1";
  parameter Real Csurf2 = C2/cst.S_sol "capacité thermique surfacique de la couche de sol 2";
  parameter Real Csurf3 = C3/cst.S_sol "capacité thermique surfacique de la couche de sol 3";
  parameter Real Csurf4 = C4/cst.S_sol "capacité thermique surfacique de la couche de sol 4";
  parameter Real Csurf5 = C5/cst.S_sol "capacité thermique surfacique de la couche de sol 5";
  parameter Real Csurf6 = C6/cst.S_sol "capacité thermique surfacique de la couche de sol 6";
  parameter Real Csurf7 = C7/cst.S_sol "capacité thermique surfacique de la couche de sol 7";

  
  Modelica.Units.SI.Temperature T(start=278.15)  "température du sol 1";
  Modelica.Units.SI.Temperature T2(start=278.15)  "température du sol 2";
  Modelica.Units.SI.Temperature T3(start=278.15)  "température du sol 3";
  Modelica.Units.SI.Temperature T4(start=278.15)  "température du sol 4";
  Modelica.Units.SI.Temperature T5(start=278.15)  "température du sol 5";
  Modelica.Units.SI.Temperature T6(start=278.15)  "température du sol 6";
  Modelica.Units.SI.Temperature T7(start=278.15)  "température du sol 7";
  parameter Modelica.Units.SI.Temperature T_profondeur = 286.65 "température thermostat à 2.5m de profondeur";
  
  Real q_cond_sol_12;
  Real q_cond_sol_23;
  Real q_cond_sol_34;
  Real q_cond_sol_45;
  Real q_cond_sol_56;
  Real q_cond_sol_67;
  Real q_cond_sol_78;
  
  parameter Modelica.Units.SI.Density rho_floor = 0.3 "masse volumique du sol";
  parameter Modelica.Units.SI.ThermalConductivity lamda_sol = 0.85 "conductivité thermique du sol";
  outer Constantes cst annotation(
    Placement(transformation(origin = {-50, 36}, extent = {{-10, -10}, {10, 10}})));
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-74, 14}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {2, 30}, extent = {{-10, -10}, {10, 10}})));

equation
  
  q_cond_sol_12 = U_floor*(T-T2);
  q_cond_sol_23 = U2_floor*(T2-T3);
  q_cond_sol_34 = U3_floor*(T3-T4);
  q_cond_sol_45 = U4_floor*(T4-T5);
  q_cond_sol_56 = U5_floor*(T5-T6);
  q_cond_sol_67 = U6_floor*(T6-T7);
  q_cond_sol_78 = U7_floor*(T7-T_profondeur);
  
  T = port_a.T;
   Csurf*der(T)=port_a.Qs_flow - q_cond_sol_12;
   Csurf2*der(T2)=q_cond_sol_12 - q_cond_sol_23;
   Csurf3*der(T3)=q_cond_sol_23 - q_cond_sol_34;
   Csurf4*der(T4)=q_cond_sol_34 - q_cond_sol_45;
   Csurf5*der(T5)=q_cond_sol_45 - q_cond_sol_56;
   Csurf6*der(T6)=q_cond_sol_56 - q_cond_sol_67;
   Csurf7*der(T7)=q_cond_sol_67 - q_cond_sol_78;
  

annotation(
    Icon(graphics = {Rectangle(origin = {0, 14}, fillColor = {170, 85, 0}, fillPattern = FillPattern.Solid, extent = {{-66, 8}, {66, -8}}), Rectangle(origin = {0, -6}, lineColor = {112, 56, 0}, fillColor = {121, 61, 0}, fillPattern = FillPattern.Solid, extent = {{-66, 12}, {66, -12}}), Rectangle(origin = {0, -37}, lineColor = {112, 56, 0}, fillColor = {94, 47, 0}, fillPattern = FillPattern.Solid, extent = {{-66, 19}, {66, -19}})}));
end Sol;
