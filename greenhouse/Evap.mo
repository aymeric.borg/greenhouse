within greenhouse;

model Evap
  Soleil soleil annotation(
    Placement(transformation(origin = {-78, -8}, extent = {{-10, -10}, {10, 10}})));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-24, 56}, extent = {{-10, -10}, {10, 10}})));
  Evapotranspiration evapotranspiration annotation(
    Placement(transformation(origin = {16, 0}, extent = {{-10, -10}, {10, 10}})));
  Plant plant annotation(
    Placement(transformation(origin = {58, -6}, extent = {{-10, -10}, {10, 10}})));
  Radiation_soleil_plant radiation_soleil_plant annotation(
    Placement(transformation(origin = {-32, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Air_int air_int annotation(
    Placement(transformation(origin = {88, 70}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_int_plant convection_air_int_plant annotation(
    Placement(transformation(origin = {78, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
equation
  connect(soleil.PAR, radiation_soleil_plant.PAR) annotation(
    Line(points = {{-66, -2}, {-42, -2}}, color = {0, 0, 127}));
  connect(soleil.NIR, radiation_soleil_plant.NIR) annotation(
    Line(points = {{-66, -12}, {-42, -12}, {-42, -10}}, color = {0, 0, 127}));
  connect(radiation_soleil_plant.Rad_tot, evapotranspiration.Rad_tot) annotation(
    Line(points = {{-22, -6}, {-8.5, -6}, {-8.5, 0}, {7, 0}}, color = {0, 0, 127}));
  connect(radiation_soleil_plant.port_b, plant.port_a) annotation(
    Line(points = {{-32, -16}, {58, -16}}, color = {191, 0, 0}));
  connect(plant.port_a, convection_air_int_plant.solid) annotation(
    Line(points = {{58, -16}, {78, -16}, {78, 16}}, color = {191, 0, 0}));
  connect(convection_air_int_plant.fluid, air_int.port_a) annotation(
    Line(points = {{80, 34}, {80, 70}}, color = {191, 0, 0}));
  connect(evapotranspiration.port_b, plant.port_a) annotation(
    Line(points = {{26, 0}, {44, 0}, {44, -16}, {58, -16}}, color = {191, 0, 0}));
  annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Evap;
