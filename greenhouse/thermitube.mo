within greenhouse;

model Thermitube

Modelica.Units.SI.Temperature T(start=273.15) "Température des thermitubes (en Kelvin)";
parameter Unit.HeatCapacityVol c_thermitube = 4.184*10^3 "capacité thermique volumique du thermitube ((constitué en grande partie d'eau)";
parameter Real C = c_thermitube/cst.D_thermitube "capacité thermique surfacique du thermitube";
Real C_thermitube = c_thermitube*19*50*33/cst.S_sol "capacité thermique surfacique des thermitubes de la serre (33L d'eau par mètre de thermitube)";

outer Constantes cst annotation(
    Placement(transformation(origin = {-50, 36}, extent = {{-10, -10}, {10, 10}})));
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-80, -2}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-80, -2}, extent = {{-10, -10}, {10, 10}})));

equation
T = port_a.T;
C*der(T)=port_a.Qs_flow;

annotation(
    Icon(graphics = {Rectangle(origin = {-1, 13}, fillPattern = FillPattern.Solid, extent = {{-75, 7}, {75, -7}}), Rectangle(origin = {-1, -13}, fillPattern = FillPattern.Solid, extent = {{-75, 7}, {75, -7}})}));
end Thermitube;
