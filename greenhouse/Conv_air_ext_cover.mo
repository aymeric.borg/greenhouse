within greenhouse;

model Conv_air_ext_cover
  Cover cover annotation(
    Placement(transformation(origin = {-76, 18}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_ext_cover convection_air_ext_cover annotation(
    Placement(transformation(origin = {-12, 16}, extent = {{-10, -10}, {10, 10}})));
  Air_ext air_ext annotation(
    Placement(transformation(origin = {32, 16}, extent = {{-10, -10}, {10, 10}})));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-56, -50}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(air_ext.T, convection_air_ext_cover.T) annotation(
    Line(points = {{21, 16}, {0, 16}}, color = {0, 0, 127}));
  connect(cover.port_a, convection_air_ext_cover.solid) annotation(
    Line(points = {{-66, 16}, {-22, 16}}, color = {191, 0, 0}));
  annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Conv_air_ext_cover;
