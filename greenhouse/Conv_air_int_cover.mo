within greenhouse;

model Conv_air_int_cover
  Convection_air_int_cover convection_air_int_cover annotation(
    Placement(transformation(origin = {0, 4}, extent = {{-10, -10}, {10, 10}})));
  Cover cover annotation(
    Placement(transformation(origin = {-78, 6}, extent = {{-10, -10}, {10, 10}})));
  Air_int air_int annotation(
    Placement(transformation(origin = {62, 2}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(cover.port_a, convection_air_int_cover.solid) annotation(
    Line(points = {{-68, 4}, {-10, 4}}, color = {191, 0, 0}));
  connect(convection_air_int_cover.fluid, air_int.port_a) annotation(
    Line(points = {{8, 2}, {54, 2}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Conv_air_int_cover;
