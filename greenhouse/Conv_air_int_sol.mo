within greenhouse;

model Conv_air_int_sol
  extends Modelica.Icons.Example;
  Air_int air_int annotation(
    Placement(transformation(origin = {28, 48}, extent = {{-10, -10}, {10, 10}})));
  Sol sol(T_profondeur(displayUnit = "mK"))  annotation(
    Placement(transformation(origin = {-20, -8}, extent = {{-10, -10}, {10, 10}})));
  Convection_air_int_sol convection_air_int_sol annotation(
    Placement(transformation(origin = {-20, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-86, 86}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(convection_air_int_sol.solid, sol.port_a) annotation(
    Line(points = {{-20, 18}, {-20, -4}}, color = {191, 0, 0}));
  connect(convection_air_int_sol.fluid, air_int.port_a) annotation(
    Line(points = {{-20, 38}, {-20, 49}, {20, 49}, {20, 48}}, color = {191, 0, 0}));

annotation(
    Diagram);
end Conv_air_int_sol;
