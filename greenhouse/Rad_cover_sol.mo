within greenhouse;

model Rad_cover_sol
  Radiation_cover_sol radiation_cover_sol annotation(
    Placement(transformation(origin = {-10, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Sol sol annotation(
    Placement(transformation(origin = {-10, -40}, extent = {{-10, -10}, {10, 10}})));
  Cover cover annotation(
    Placement(transformation(origin = {-28, 62}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(sol.port_a, radiation_cover_sol.port_a) annotation(
    Line(points = {{-10, -36}, {-10, -6}}, color = {191, 0, 0}));
  connect(radiation_cover_sol.port_b, cover.port_a) annotation(
    Line(points = {{-10, 14}, {-10, 59}, {-19, 59}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_cover_sol;
