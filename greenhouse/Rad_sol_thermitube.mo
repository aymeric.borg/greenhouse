within greenhouse;

model Rad_sol_thermitube
  Thermitube thermitube annotation(
    Placement(transformation(origin = {48, -2}, extent = {{-10, -10}, {10, 10}})));
  Radiation_sol_thermitube radiation_sol_thermitube annotation(
    Placement(transformation(origin = {0, -2}, extent = {{-10, -10}, {10, 10}})));
  Sol sol annotation(
    Placement(transformation(origin = {-60, -6}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(sol.port_a, radiation_sol_thermitube.port_a) annotation(
    Line(points = {{-60, -3}, {-10, -3}, {-10, -2}}, color = {191, 0, 0}));
  connect(radiation_sol_thermitube.port_b, thermitube.port_a) annotation(
    Line(points = {{10, -2}, {40, -2}}, color = {191, 0, 0}));
  annotation(
    Icon(graphics = {Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_sol_thermitube;
