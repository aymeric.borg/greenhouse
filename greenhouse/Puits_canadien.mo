within greenhouse;

model Puits_canadien
  Conv_puits_canadien puits annotation(
    Placement(transformation(origin = {0, 2}, extent = {{-10, -10}, {10, 10}})));
  Air_int air_int annotation(
    Placement(transformation(origin = {-78, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Blocks.Sources.Constant T_air_sol(k = 13.5)  annotation(
    Placement(transformation(origin = {48, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-14, 62}, extent = {{-10, -10}, {10, 10}})));
  Switch switch annotation(
    Placement(transformation(origin = {-30, 2}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(puits.T_sol, T_air_sol.y) annotation(
    Line(points = {{12, 2}, {38, 2}}, color = {0, 0, 127}));
  connect(switch.port_a, air_int.port_a) annotation(
    Line(points = {{-40, 2}, {-70, 2}}, color = {191, 0, 0}));
  connect(switch.port_b, puits.solid) annotation(
    Line(points = {{-20, 2}, {-10, 2}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Puits_canadien;
