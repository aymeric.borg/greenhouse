within greenhouse;

model Rad_soleil_thermitube
  Radiation_soleil_thermitube radiation_soleil_thermitube annotation(
    Placement(transformation(origin = {-2, -2}, extent = {{-10, -10}, {10, 10}})));
  Soleil soleil annotation(
    Placement(transformation(origin = {-4, 56}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Thermitube thermitube annotation(
    Placement(transformation(origin = {-62, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
equation
  connect(soleil.NIR, radiation_soleil_thermitube.NIR) annotation(
    Line(points = {{-8, 46}, {-6, 46}, {-6, 8}}, color = {0, 0, 127}));
  connect(soleil.PAR, radiation_soleil_thermitube.PAR) annotation(
    Line(points = {{2, 46}, {2, 8}}, color = {0, 0, 127}));
  connect(thermitube.port_a, radiation_soleil_thermitube.port_b) annotation(
    Line(points = {{-54, -2}, {-12, -2}}, color = {191, 0, 0}));
annotation(
    Icon(graphics = {Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_soleil_thermitube;
