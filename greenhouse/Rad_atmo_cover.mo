within greenhouse;

model Rad_atmo_cover
  Cover cover annotation(
    Placement(transformation(origin = {-60, -6}, extent = {{-10, -10}, {10, 10}})));
  Modelica.Blocks.Sources.Constant T_sky(k = 5)  annotation(
    Placement(transformation(origin = {62, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Radiation_atmo_cover radiation_atmo_cover annotation(
    Placement(transformation(origin = {-8, -10}, extent = {{-10, -10}, {10, 10}})));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-58, 60}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(cover.port_a, radiation_atmo_cover.port_a) annotation(
    Line(points = {{-50, -8}, {-18, -8}, {-18, -10}}, color = {191, 0, 0}));
  connect(radiation_atmo_cover.T, T_sky.y) annotation(
    Line(points = {{2, -10}, {52, -10}}, color = {0, 0, 127}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_atmo_cover;
