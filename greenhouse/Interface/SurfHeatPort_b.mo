within greenhouse.Interface;

connector SurfHeatPort_b
  "Thermal port for 1-dim. heat transfer (filled rectangular icon)"

  Modelica.Units.SI.Temperature T "Port temperature";
  flow Modelica.Units.SI.HeatFlux Qs_flow
    "Surface heat flow rate (positive if flowing from outside into the component)";
  annotation(defaultComponentName = "port_b",
                                        Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={191,0,0},
          fillColor={191,0,0},
          fillPattern=FillPattern.Solid)}),

          
                                        Diagram(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},
            {100,100}}), graphics={Rectangle(
          extent={{-50,50},{50,-50}},
          lineColor={191,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-100,120},{120,60}},
          textColor={191,0,0},
          textString="%name")}),
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={191,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid)}));
end SurfHeatPort_b;
