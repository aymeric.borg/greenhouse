within greenhouse.Interface;

connector SurfHeatPort_a
  "Thermal port for 1-dim. heat transfer (filled rectangular icon)"

  Modelica.Units.SI.Temperature T "Port temperature";
  flow Modelica.Units.SI.HeatFlux Qs_flow
    "Surface heat flow rate (positive if flowing from outside into the component)";
  annotation(defaultComponentName = "port_a",
                                        Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={191,0,0},
          fillColor={191,0,0},
          fillPattern=FillPattern.Solid)}),
    Diagram(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},
            {100,100}}), graphics={Rectangle(
          extent={{-50,50},{50,-50}},
          lineColor={191,0,0},
          fillColor={191,0,0},
          fillPattern=FillPattern.Solid), Text(
          extent={{-120,120},{100,60}},
          textColor={191,0,0},
          textString="%name")}));

end SurfHeatPort_a;
