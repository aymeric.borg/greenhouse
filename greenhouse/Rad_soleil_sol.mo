within greenhouse;

model Rad_soleil_sol
  Radiation_soleil_sol radiation_soleil_sol annotation(
    Placement(transformation(origin = {2, -2}, extent = {{-10, -10}, {10, 10}})));
  Soleil soleil annotation(
    Placement(transformation(origin = {0, 54}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Sol sol annotation(
    Placement(transformation(origin = {2, -40}, extent = {{-10, -10}, {10, 10}})));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-62, -52}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(soleil.NIR, radiation_soleil_sol.NIR) annotation(
    Line(points = {{-4, 43}, {-4, 26.5}, {-2, 26.5}, {-2, 8}}, color = {0, 0, 127}));
  connect(soleil.PAR, radiation_soleil_sol.PAR) annotation(
    Line(points = {{6, 43}, {6, 8}}, color = {0, 0, 127}));
  connect(radiation_soleil_sol.port_b, sol.port_a) annotation(
    Line(points = {{2, -12}, {2, -36}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_soleil_sol;
