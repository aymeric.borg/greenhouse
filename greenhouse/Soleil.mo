within greenhouse;

model Soleil "Rayonnement PAR et NIR" 
  parameter String irradFile = Modelica.Utilities.Files.loadResource("modelica://greenhouse/I_GLOB_MODELICA.txt");
  Modelica.Blocks.Interfaces.RealOutput PAR annotation(
    Placement(transformation(origin = {110, 40}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {110, 58}, extent = {{-10, -10}, {10, 10}})));
  Modelica.Blocks.Interfaces.RealOutput NIR annotation(
    Placement(transformation(origin = {110, -40}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {110, -40}, extent = {{-10, -10}, {10, 10}})));
  Modelica.Blocks.Sources.CombiTimeTable combiTimeTable(tableOnFile = true, tableName = "table", fileName = irradFile, timeScale = 3600)  annotation(
    Placement(transformation(origin = {-90, 0}, extent = {{-10, -10}, {10, 10}})));
 Modelica.Blocks.Math.Gain gain(k = 0.1)  annotation(
    Placement(transformation(origin = {10, 40}, extent = {{-10, -10}, {10, 10}})));
 Modelica.Blocks.Math.Gain gain1(k = 0.1)  annotation(
    Placement(transformation(origin = {10, -40}, extent = {{-10, -10}, {10, 10}})));
equation
 connect(gain.y, PAR) annotation(
    Line(points = {{21, 40}, {110, 40}}, color = {0, 0, 127}));
 connect(gain.u, combiTimeTable.y[1]) annotation(
    Line(points = {{-2, 40}, {-40, 40}, {-40, 0}, {-78, 0}}, color = {0, 0, 127}));
 connect(gain1.y, NIR) annotation(
    Line(points = {{21, -40}, {110, -40}}, color = {0, 0, 127}));
 connect(gain1.u, combiTimeTable.y[1]) annotation(
    Line(points = {{-2, -40}, {-40, -40}, {-40, 0}, {-78, 0}}, color = {0, 0, 127}));   
annotation(
    uses(Modelica(version = "4.0.0")),
 Icon(graphics = {Ellipse(lineColor = {255, 170, 0}, fillColor = {255, 255, 0}, fillPattern = FillPattern.Solid, extent = {{-80, 80}, {80, -80}})}));
end Soleil;
