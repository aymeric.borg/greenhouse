within greenhouse;

model Rad_soleil_plant
  Radiation_soleil_plant radiation_soleil_plant annotation(
    Placement(transformation(origin = {-18, 2}, extent = {{-10, -10}, {10, 10}})));
  Plant plant annotation(
    Placement(transformation(origin = {-60, 10}, extent = {{-10, -10}, {10, 10}})));
  Soleil soleil annotation(
    Placement(transformation(origin = {-20, 52}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  inner Constantes cst annotation(
    Placement(transformation(origin = {-84, 82}, extent = {{-10, -10}, {10, 10}})));
equation

  connect(soleil.NIR, radiation_soleil_plant.NIR) annotation(
    Line(points = {{-24, 41}, {-24, 26.5}, {-22, 26.5}, {-22, 12}}, color = {0, 0, 127}));
  connect(soleil.PAR, radiation_soleil_plant.PAR) annotation(
    Line(points = {{-14, 41}, {-14, 12}}, color = {0, 0, 127}));
  connect(radiation_soleil_plant.port_b, plant.port_a) annotation(
    Line(points = {{-28, 2}, {-38, 2}, {-38, -12}, {-78, -12}, {-78, 10}, {-68, 10}}, color = {191, 0, 0}));

annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));
end Rad_soleil_plant;
