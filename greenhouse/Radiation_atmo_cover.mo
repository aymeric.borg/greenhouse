within greenhouse;

model Radiation_atmo_cover
  Modelica.Units.SI.HeatFlux Qs_flow;
  Interface.SurfHeatPort_a port_a annotation(
    Placement(transformation(origin = {-94, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-102, 0}, extent = {{-10, -10}, {10, 10}})));
  Real Gc = cst.eps_cover*cst.eps_atmo*cst.F_cover_sky*Modelica.Constants.sigma;
  Modelica.Blocks.Interfaces.RealInput T(unit="degC") annotation (Placement(transformation(origin = {92, 14}, extent = {{-20, -20}, {20, 20}}, rotation = 270), iconTransformation(origin = {100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
  
  outer Constantes cst;
equation
  port_a.Qs_flow=Qs_flow;
    Qs_flow=Gc*(port_a.T^4-(T + 273.15)^4);
annotation(
    Icon(graphics = {Line(origin = {-0.45, 59.55}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {-0.8, -19.9}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {1.65, -60.15}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Line(origin = {1.65, 19.65}, points = {{-59, 0}, {59, 0}, {59, 0}, {59, 0}}), Text(origin = {48, 103}, extent = {{-24, 13}, {24, -13}}, textString = "eps"), Text(origin = {104, -29}, extent = {{-24, 13}, {24, -13}}, textString = "T_sky")}));

end Radiation_atmo_cover;
