Projet Modelica-Simulation de la thermique d'une serre

Par Aymeric Borg, Rémi Cuvelier, Antoine Perla, Malo Champigny

Le projet a pour objectif de modeliser les différents composants d'une serre et les interactions entre ceux-ci afin d'étudier la thermique de la serre.
Les composants modélisés sont la paroi de la serre, les plantes à l'intérieur, le sol (décomposé en plusieurs couches) et l'air à l'intérieur de la serre. Ils interragissent ensemble via des échanges radiatifs, convectifs et convectifs. L'évolution de leur température est également influencée par le soleil, l'air à l'extérieur de la serre et l'atmosphère terrestre.

L'ensemble des modèlees réalisés a été placé au sein d'un package dans Modelica afin de pouvoir les connecter entre eux dans un modèle rassemblant tous les sous-modèles. Il est possible de lancer une simulation de chaque sous-modèle de manière indépendante, pour s'assurer de leur bon fonctionnement ou observer l'influence de certains paramètres.

Pour effectuer une simulation, l'utilisateur doit ouvrir le fichier package situé dans le dossier greenhouse.